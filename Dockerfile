FROM registry.gitlab.com/fdroid/docker-executable-fdroidserver:master

ENV ANDROID_HOME=/opt/android-sdk
ENV PATH="$PATH:/home/vagrant/fdroidserver"

RUN apt -y update && \
    apt -y install jq yq python3-yattag && \
    sdkmanager --install "build-tools;34.0.0" && \
    sdkmanager --install "platforms;android-34" && \
    sdkmanager --install "platform-tools"
