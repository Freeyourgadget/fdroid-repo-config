#!/bin/sh

reporoot="${CI_WORKSPACE}"
cd ${reporoot}
tag=$(git -C "${reporoot}" rev-parse --short HEAD)
name="${CI_COMMIT_BRANCH} - ${CI_COMMIT_MESSAGE} - $tag"
root="${CI_WORKSPACE%%${CI_REPO_NAME}}"
repo="Freeyourgadget/pages"

pwd
echo "Creating a new release"

log=$(git -C "${reporoot}" log  --pretty=format:'%h - %s ' -n 5 | sed -e s/\"/\'/g | awk '{printf "%s\\n", $0}' )

echo "---------------"
echo ${log}
echo "---------------"

echo ${#log}

json_data=$(cat << EOF
{
  "body": "This is a testing Gadgetbridge release created for some special purpose. Do not install unless advised! Only the attached apk is relevant and to be used. The source code is in the official Gadgetbridge repo, the code tarballs attached here are unrelated to this release.\n\nCommits:\n${log}",
  "draft": false,
  "name": "${name}",
  "prerelease": true,
  "tag_name": "${tag}",
  "target_commitish": "HEAD"}
EOF
)

#echo ${json_data}

curl -X 'POST' \
  'https://codeberg.org/api/v1/repos/'${repo}'/releases' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -H "Authorization: token ${COMMIT_TOKEN}" \
  -d "${json_data}" | jq '.' > ./response.json

#jq '.' ./response.json

id=$(jq -r '.id' ./response.json)
url=$(jq -r '.html_url' ./response.json)

echo "Release id: " "${id}" " has been created, adding the app-main-debug.apk"

cd ${reporoot}/app/build/outputs/apk/mainline/debug/
pwd
ls

curl -X 'POST' \
  'https://codeberg.org/api/v1/repos/'${repo}'/releases/'"${id}"'/assets?name=app-main-debug.apk' \
  -H 'accept: application/json' \
  -H "Authorization: token ${COMMIT_TOKEN}" \
  -H 'Content-Type: multipart/form-data' \
  -F 'attachment=@app-main-debug.apk;type=application/vnd.android.package-archive'

echo "Clickable link to the release page:"
${reporoot}/../repoconfig/fdroid-repo-config/repoconfig/hyperlink.sh ${url}
