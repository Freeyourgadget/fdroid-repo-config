#!/bin/sh
cd Gadgetbridge
git pull upstream nightly-branch
git pull upstream master
git rebase upstream/master && git push -f upstream nightly-branch
export WOODPECKER_SERVER="https://ci.codeberg.org"
export WOODPECKER_TOKEN="token"

number=$(../woodpecker build ls Freeyourgadget/Gadgetbridge --branch nightly-branch --limit 1 --filter blocked --format "#{{.Number}}")
num=$(echo $number | sed -e 's/#//g')
echo $num
../woodpecker build approve Freeyourgadget/Gadgetbridge ${num}
