#!/bin/env python

from yattag import Doc
from yattag import indent
import json
from datetime import datetime

with open("./repo/index-v1.json") as file:
    data = json.load(file)

doc, tag, text, line = Doc().ttl()

# doc.stag("link", href="index.css", rel="stylesheet", type="text/css")
with tag("div", klass="repoapplist"):
    for app in data["apps"]:
        with tag("div", klass="approw"):
            appdata = app["localized"]["en-US"]
            with tag("div", klass="appiconbig"):
                doc.stag(
                    "img",
                    src=f"{app['packageName']}/en-US/{appdata['icon']}",
                    alt="icon",
                    width="48",
                )

            with tag("div", klass="appdetailblock"):
                with tag("div", klass="appdetailinner"):
                    with tag("div", klass="appdetailrow"):
                        with tag("div", klass="appdetailcell"):
                            with tag("span", klass="boldname"):
                                text(appdata.get("name", "no name"))

                        with tag("div", klass="appdetailcell"):
                            with tag("span", klass="minor-details"):
                                ts = datetime.fromtimestamp(app["lastUpdated"] / 1000)
                                fts = ts.strftime("%Y-%b-%d %H:%M:%S")
                                text(f"{fts} UTC")
                                text(
                                    f" / {data['packages'][app['packageName']][0].get('versionCode', 'no version code')}"
                                )
                                text(
                                    f" / {data['packages'][app['packageName']][0].get('versionName', 'no version name')}"
                                )

                        with tag("div", klass="appdetailcell"):
                            with tag("span", klass="minor-details"):
                                text(app["license"])
                    with tag("div", klass="appdetailrow"):
                        with tag("div", klass="appdetailcell"):
                            text(appdata["description"])
                            with tag("details", "open"):
                                with tag("summary"):
                                    text("Recent changes")
                                with tag("ul"):
                                    for line in appdata.get("whatsNew", "").split("\n"):
                                        with tag("li"):
                                            text(line)

                    with tag("div", klass="appdetailrow"):
                        with tag("div", klass="appdetailcell applinkcell"):
                            with tag("center"):

                                with tag(
                                    "a",
                                    klass="paddedlink",
                                    href=f"{data['packages'][app['packageName']][0].get('apkName','')}",
                                ):
                                    text("Download")

                                with tag(
                                    "a",
                                    klass="paddedlink",
                                    href=f'{app.get("sourceCode", "")}',
                                ):
                                    text("Source")
                                with tag(
                                    "a",
                                    klass="paddedlink",
                                    href=f'{app.get("sourceCode", "")}/commits/branch/master',
                                ):
                                    text("All Commits")

print(doc.getvalue())
# print(indent(doc.getvalue(), indentation="  ", newline="\n", indent_text=True))
