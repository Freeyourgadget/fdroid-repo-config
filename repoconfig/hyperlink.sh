#!/bin/bash

# this script makes a weblink clickable message to the terminal
# based on this escape sequence: https://gist.github.com/egmontkob/eb114294efbcd5adb1944c9f3cb5feda#the-escape-sequence

printf '\e]8;;%s\e\\%s\e]8;;\e\\\n' "$1" "${2:-$1}";
