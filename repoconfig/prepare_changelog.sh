#!/bin/sh
root="${CI_WORKSPACE%%${CI_REPO_NAME}}"
reporoot="${CI_WORKSPACE}"
cnt=$( git rev-list HEAD --count )
echo ${cnt}

lines=$(git -C "${reporoot}" log --pretty=format:'%s' --since="1 day ago" | sed -e s/\"/\'/g | wc -l)

if [ "$lines" -gt 3 ]; then
    git -C "${reporoot}" log --pretty=format:'%s' --since="1 day ago" > "${root}""/repoconfig/fdroid-repo-config/repoconfig/metadata/nodomain.freeyourgadget.gadgetbridge.nightly/android/en-US/changelogs/"${cnt}.txt
    git -C "${reporoot}" log --pretty=format:'%s' --since="1 day ago" > "${root}""/repoconfig/fdroid-repo-config/repoconfig/metadata/nodomain.freeyourgadget.gadgetbridge.nightly_nopebble/android/en-US/changelogs/"${cnt}.txt
    git -C "${reporoot}" log --pretty=format:'%s' --since="1 day ago" > "${root}""/repoconfig/fdroid-repo-config/repoconfig/metadata/com.espruino.gadgetbridge.banglejs.nightly/android/en-US/changelogs/"${cnt}.txt
else
    git -C "${reporoot}" log --pretty=format:'%s' -n 5 > "${root}""/repoconfig/fdroid-repo-config/repoconfig/metadata/nodomain.freeyourgadget.gadgetbridge.nightly/android/en-US/changelogs/"${cnt}.txt
    git -C "${reporoot}" log --pretty=format:'%s' -n 5 > "${root}""/repoconfig/fdroid-repo-config/repoconfig/metadata/nodomain.freeyourgadget.gadgetbridge.nightly_nopebble/android/en-US/changelogs/"${cnt}.txt
    git -C "${reporoot}" log --pretty=format:'%s' -n 5 > "${root}""/repoconfig/fdroid-repo-config/repoconfig/metadata/com.espruino.gadgetbridge.banglejs.nightly/android/en-US/changelogs/"${cnt}.txt
fi
