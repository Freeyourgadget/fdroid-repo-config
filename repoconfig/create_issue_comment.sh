#!/bin/sh

# this is to report a message to a specific issue

repo="vanous/Gadgetbridge"
issue="12"

echo ${message}

curl -X 'POST' \
  'https://codeberg.org/api/v1/repos/'${repo}'/issues/'${issue}'/comments' \
  -H 'accept: application/json' \
  -H "Authorization: token ${COMMIT_TOKEN}" \
  -H 'Content-Type: application/json' \
  -d '{"body":"'"$1"'"}'
