#!/usr/bin/env python3
#
import os
from argparse import ArgumentParser
from fdroidserver import common
from fdroidserver import signindex

def main():
    parser = ArgumentParser()
    common.setup_global_opts(parser)
    options = parser.parse_args()
    config = common.read_config(options)
    signindex.config = config
    signindex.sign_index_v1("repo", "index-v1.json")

if __name__ == "__main__":
    main()
