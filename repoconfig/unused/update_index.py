#!/usr/bin/env python3
#
import os
import json
from datetime import datetime

def main():

    with open("./repo/index-v1.json") as file:
        data = json.load(file)

    for app in data["apps"]:
        pass

    for package, package_data in data["packages"].items():
        for pkg in package_data:
            #pkg=package_data[0]
            packageApkName = pkg.get("apkName", "")
            pkg["apkName"]= f"../../{packageApkName}"

    with open("./repo/index-v1.json", "w") as file:
        file.write(json.dumps(data))

if __name__ == "__main__":
    main()
