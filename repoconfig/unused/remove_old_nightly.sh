#!/bin/sh

repo="${CI_REPO}"
tag="nightly"

release_id=$(curl -X 'GET' \
  'https://codeberg.org/api/v1/repos/'${repo}'/releases' \
  -H 'accept: application/json' | jq -c '[ .[] | select( .tag_name | contains("'${tag}'")) ]' | jq '.[] .id')

echo "Removing release id: " "${release_id}"

curl -X 'DELETE' \
  'https://codeberg.org/api/v1/repos/'${repo}'/releases/'"${release_id}" \
  -H 'accept: application/json' \
  -H "Authorization: token ${COMMIT_TOKEN}"

echo "Removing nightly tag"

curl -X 'DELETE' \
  'https://codeberg.org/api/v1/repos/'${repo}'/tags/'${tag} \
  -H 'accept: application/json' \
  -H "Authorization: token ${COMMIT_TOKEN}"
