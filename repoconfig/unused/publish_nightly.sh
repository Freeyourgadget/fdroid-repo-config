#!/bin/sh

tag="nightly"
root="${CI_WORKSPACE%%${CI_REPO_NAME}}"
reporoot="${CI_WORKSPACE}"
repo="Freeyourgadget/fdroid-repo-config"

echo "Creating a new release"

lines=$(git -C "${reporoot}" log --pretty=format:'%h - %s ' --since="1 day ago" | sed -e s/\"/\'/g | wc -l)

if [ "$lines" -gt 3 ]; then
    log=$(git -C "${reporoot}" log --pretty=format:'%h - %s ' --since="1 day ago" | sed -e s/\"/\'/g | awk '{printf "%s\\n", $0}' )
else
    log=$(git -C "${reporoot}" log --pretty=format:'%h - %s ' -n 5 | sed -e s/\"/\'/g | awk '{printf "%s\\n", $0}' )
fi

echo "---------------"
echo ${log}
echo "---------------"

echo ${#log}

json_data=$(cat << EOF
{
  "body": "Nightly is only released if there have been commits to the upstream repo. There are two debug builds:\n - debug.apk, which can only be installed if Gadgetbridge has not been installed on the phone.\n - debug-nopebble.apk, which can be installed alongside the existing Gadgetbridge installation, but Pebble integration will not work in this release.\n\nCommits:\n${log}",
  "draft": false,
  "name": "nightly release",
  "prerelease": true,
  "tag_name": "${tag}",
  "target_commitish": "HEAD"}
EOF
)

echo ${json_data}

id=$(curl -X 'POST' \
  'https://codeberg.org/api/v1/repos/'${repo}'/releases' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -H "Authorization: token ${COMMIT_TOKEN}" \
  -d "${json_data}" | jq '.id')

echo "Release id: " "${id}" " has been created, adding the debug.apk"

cd ${reporoot}/app/build/outputs/apk/mainline/nightly/
nightly=$(ls *.apk)
echo ${nightly}
cp ${nightly} ${root}repoconfig/fdroid-repo-config/repoconfig/repo

curl -X 'POST' \
  'https://codeberg.org/api/v1/repos/'${repo}'/releases/'"${id}"'/assets?name=debug.apk' \
  -H 'accept: application/json' \
  -H "Authorization: token ${COMMIT_TOKEN}" \
  -H 'Content-Type: multipart/form-data' \
  -F 'attachment=@'"${nightly}"';type=application/vnd.android.package-archive'


cd ${reporoot}/app/build/outputs/apk/mainline/nopebble/
nopebble=$(ls *.apk)
echo ${nopebble}
cp ${nopebble} ${root}repoconfig/fdroid-repo-config/repoconfig/repo

curl -X 'POST' \
  'https://codeberg.org/api/v1/repos/'${repo}'/releases/'"${id}"'/assets?name=debug-nopebble.apk' \
  -H 'accept: application/json' \
  -H "Authorization: token ${COMMIT_TOKEN}" \
  -H 'Content-Type: multipart/form-data' \
  -F 'attachment=@'"${nopebble}"';type=application/vnd.android.package-archive'

pwd

