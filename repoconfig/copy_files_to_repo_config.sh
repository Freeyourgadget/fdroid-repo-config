#!/bin/sh

root="${CI_WORKSPACE%%${CI_REPO_NAME}}"
reporoot="${CI_WORKSPACE}"

cd ${reporoot}/app/build/outputs/apk/mainline/nightly/
nightly=$(ls *.apk)
echo ${nightly}
cp ${nightly} ${root}repoconfig/fdroid-repo-config/repoconfig/repo

cd ${reporoot}/app/build/outputs/apk/mainline/nopebble/
nopebble=$(ls *.apk)
echo ${nopebble}
cp ${nopebble} ${root}repoconfig/fdroid-repo-config/repoconfig/repo

cd ${reporoot}/app/build/outputs/apk/banglejs/nightly/
banglejs=$(ls *.apk)
echo ${banglejs}
cp ${banglejs} ${root}repoconfig/fdroid-repo-config/repoconfig/repo
