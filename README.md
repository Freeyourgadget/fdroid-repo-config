# Nightly F-Droid repository for Gadgetbridge

- self hosted via Codeberg pages
- every push soft resets previous commit to keep the pages repo from growing HUGE
- compiled and pushed via Codeberg CI (Woodpecker), currently in testing mode
- configuration is in this repo
- hosting is in pages repo https://codeberg.org/Freeyourgadget/pages
- F-Droid repository is actually here: https://freeyourgadget.codeberg.page/fdroid/repo/
- Docker image used in the build steps

## Files in the repoconfig

**Used**

These files are used by the CI/CD from the nightly-branch of Gadgetbridge.

- repoconfig/prepare_changelog.sh - prepares changelog for the list of changes in F-Droid client
- repoconfig/repo_listing.py - creates list of apks and their descriptions on the repo index.html page
- repoconfig/copy_files_to_repo_config.sh - copies apks to the repo config directory
- repoconfig/create_issue_comment.sh - adds a message to the issue tracker
- repoconfig/publish_debug_version.sh - used with debug-branch to publish a custom apk as a release to the pages repo
- repoconfig/hyperlink.sh - make clickable url in the console (on woodpecker task output)
- Dockerfile - definition for the custom docker image that is used in (all) build steps
- .woodpecker.yml - CI steps to create and publish the Docker image into the [Package repository](https://codeberg.org/Freeyourgadget/fdroid-repo-config/packages).

**Used on a remote server**

- repoconfig/sync-gb.sh - this script is run via cron on another server to update the nightly-branch and to approve the CI build. 


**Unused, kept for the record**

- repoconfig/unused/remove_old_nightly.sh -removes the release tag from the releases in pages repo
- repoconfig/unused/publish_nightly.sh - creates a new release and uploads apks to the releases in pages repo
- repoconfig/unused/update_index.py and repoconfig/resign_index.py - Can change index files and re-sign them with the keyfile

## Docker image

The Docker image we use is based on [fdroid/docker-executable-fdroidserver](https://gitlab.com/fdroid/docker-executable-fdroidserver) image, it contains Android SDK and fdroid publishing tool, we then add some small utilities needed for f-droid publishing. The image is built by [Codeberg's CI](https://ci.codeberg.org/Freeyourgadget/fdroid-repo-config) (the task must be manually enabled to run) and hosted in this very repo on Codeberg [here](https://codeberg.org/Freeyourgadget/fdroid-repo-config/packages).

Typically, our image is tagged `latest` but as there can be caching on Codeberg crossing our path, it is better to tag based on current tools version.

How to test:

```shell
docker build . -t gb-android-fdroid-tools
docker run -it --rm \
    --env-file ./test.env \
    -v "/storage/workspace/Freeyourgadget/Gadgetbridge/:/woodpecker/src/codeberg.org/Freeyourgadget/Gadgetbridge" \
    -v "/storage/workspace/Freeyourgadget/fdroid-repo-config/:/woodpecker/src/codeberg.org/Freeyourgadget/repoconfig/fdroid-repo-config" \
    -w /woodpecker/src/codeberg.org/Freeyourgadget/Gadgetbridge \
    --entrypoint /bin/bash \
    gb-android-fdroid-tools

# In the container
git config --global --add safe.directory /woodpecker/src/codeberg.org/Freeyourgadget/Gadgetbridge

# Then execute the commands as per the nightly pipeline
```

## Woodpecker pipeline setup:

https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/nightly-branch/.woodpecker/nightly.yml


## Woodpecker secrets

### Signing key

This key and passwords are used to sign the repo index files and to sign the nightly apks.

Clone repo `fdroid-repo-config`, make sure to have fdroidserver installed (`python -m pip install fdroidserver==2.1a0`), remove the empty key and generate a new one:

**Comment the `keystorepass` and `keypass` in the `config.yml`!**

```bash
git clone git@codeberg.org:Freeyourgadget/fdroid-repo-config.git
cd fdroid-repo-config
rm keystore.p12
fdroid update --create-key
```
This generates 3 things: `keystore.p12` keyfile, `keystorepass` and `keypass` in the `config.yml`.

Take the key and convert it to base64 for the CI/CD:

`cat keystore.p12 | base64 > out`

Add the key to Woodpecker as a secret, via woodpecker cli:

`/path_to/woodpecker secret add -repository Freeyourgadget/Gadgetbridge -name signing_key -value @out -event push`

Also, add the `keystorepass` and `keypass` into the Woodpecker secrets as `keystorepass` and `keypass`. In reality, these two passwords are the same.

Put the branch back to original state:

```bash
git restore .
git clean -f -d
```
## Commit token

Create personal access token and add this token as `commit_token` to the Woodpecker secrets. The git config to push to the pages and to the nightly-branch repo is set to:

```yaml
- git config user.name "CODEBERG CI"
- git config user.email "noreply@nodomain.nodomain"
```

## Limitations

- if we wanted to add more apps to the f-droid repo, we would need to re-build them all at the same time - we would need to clone their git repo and copy the apk to fdroid config directory. To do this without rebuilding everything, some other persistent storage would be useful.
- hosting of the apks must currently be done from the pages git repo. This could over time blow the repo huge in size, this is why each automated commit squashes with the previous commit. This could be an issue in the future. Unfortunately, the other possibility to tell F-Droid client that the apk is hosted elsewhere is not possible, see [here](https://gitlab.com/fdroid/fdroidclient/-/issues/1164), at it also cannot be done via http redirects, see [here](https://gitlab.com/fdroid/fdroidclient/-/issues/1041) (actually, from that discussion in am not clear if http redirects now work or not).
